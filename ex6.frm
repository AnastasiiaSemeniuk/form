off stat;
index mu, nu, al,bt, ln, rh; 
autodeclare symbol S, G; 
vector a, a5; 
function sigma; 
cfunction sg(anti);
 
tensor s(anti);
*local [G]=i_*g_(1, 0, 1, 2, 3);  
*local[S]=[G]*g_(1, mu, nu, ln, rh); 
local [S]=g_(1,5_)*sigma(mu, nu); 
.sort
id sigma(mu?, nu?)=i_/2*(g_(1,mu,nu)-g_(1,nu,mu)); 
print;
.sort
local [S1]= 1/4*[S];
local [S2]=1/4*[S]*g_(1,al); 
local [S3]=1/4*[S]*g5_(1); 
local [S4]=-1/4*[S]*g_(1,5_,al); 
local [S5]=1/8*[S]*sigma(al,bt); 
id sigma (mu?,nu?) =i_/2*(g_(1,mu,nu)-g_(1,nu,mu));  
trace4,1;
print;
.sort 
local [Sg]=sg(al,bt)*[S5]; 
print [Sg];
.end

