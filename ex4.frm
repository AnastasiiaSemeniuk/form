off stat;
index mu, nu, al,bt; 
autodeclare symbol S; 
vector a, a5; 
function sigma; 
tensor s(anti); 
local[S]=S1*gi_(1) + a(mu)*g_(1,mu)+S5*g5_(1)+a5(mu)*g_(1,5_,mu)+s(mu,nu)*sigma(mu,nu); 
.sort
id sigma(mu?, nu?)=i_/2*(g_(1,mu,nu)-g_(1,nu,mu)); 
print;
.sort
local [S1]= 1/4*[S];
local [S2]=1/4*[S]*g_(1,al); 
local [S3]=1/4*[S]*g5_(1); 
local [S4]=-1/4*[S]*g_(1,5_,al); 
local [S5]=1/8*[S]*sigma(al,bt); 
id sigma (mu?,nu?) =i_/2*(g_(1,mu,nu)-g_(1,nu,mu));  
trace4,1;
print; 
.end

